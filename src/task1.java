import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import  java.lang.Comparable;

public class task1 implements Comparable<Document>{
    private static final AtomicInteger numberGenerator = new AtomicInteger(1);
    public static void main(String[] args){
        ArrayList<Document> docs =new ArrayList<Document>();
        for(int i=0;i<10;i++) {
            docs.add(createDoc("Task"));
        }
        //формируем отчет
        Collections.sort(docs);
        System.out.println("Автор документа: "+docs.get(0).author);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        System.out.println("          Тип документа: "+docs.get(0).type+" №"+docs.get(0).number+"  от " +dateFormat.format(docs.get(0).date) +". Название документа: " + docs.get(0).name);

        for(int i=1;i<docs.size();i++) {
            if(docs.get(i).author!=docs.get(i-1).author) {
                System.out.println("Автор документа: "+docs.get(i).author);
            }
                System.out.println("          Тип документа: "+docs.get(i).type+" №"+docs.get(i).number+"  от " +dateFormat.format(docs.get(i).date) +". Название документа: " + docs.get(i).name);

        }

    }
    public static Document createDoc(String classDoc){
        Document Document;
        String[] listAuthor= {"Полищук Шарль Евгеньевич","Дьячков Радислав Петрович","Мамонтов Чеслав Платонович","Кулагин Максим Фёдорович","Федосеев Аполлон Данилович"};
        String[] listName= {"Срочно","Важно","Зарплата","План работы","Выходной"};
        switch(classDoc){
            case "Task":
                Document=new Task() {
                };
                Document.type = "Task";
                break;
            case "Incoming":
                Document = new Incoming() {
                };
                Document.type = "Incoming";
                break;
            case "Outgoing":
                Document= new Outgoing() {
                };
                Document.type = "Outgoing";
                break;
            default:
                Document = new Document() {

                    @Override
                    public int compareTo(Document o) {
                        return 0;
                    }
                };
                break;
        }
        Document.ID=(int)Math.random()*1000;
        Document.name=listName[new Random().nextInt(listName.length)];
        Document.text="Случайная генерация теста="+(int)(Math.random()*100000);
        Document.number=numberGenerator.getAndIncrement();
        Document.author= listAuthor[new Random().nextInt(listAuthor.length)];
        Document.date=new Date(new Random().nextLong(1000000000));
        return Document;
    }

    @Override
    public int compareTo(Document doc) {

        return 0;
    }
}
 abstract class Document implements Comparable<Document>,Storable{
    int ID;//•	идентификатор документа;
    String type;
    String name;//•	название документа;
    String text;//•	текст документа;
    int number;//•	регистрационный номер документа;
    Date date;//•	дата регистрации документа;
    String author;//•	автор документа.
    @Override
    public String toString() {
        return text;
    }
    @Override
    public int compareTo(Document doc) {// сортировка по автору и имени
        int result = this.author.compareTo(doc.author);// по автору
        if(result != 0) {
            return result;
        }else{
            result = this.name.compareTo(doc.name);// по имени
            return result;
        }
    }

     @Override
     public void getNumber() {

     }

     @Override
     public void getName() {

     }
 }
 class Task extends Document{
    Date orderDate;//•	дата выдачи поручения;
    Date deadline;//•	срок исполнения поручения;
    String executor;//•	ответственный исполнитель;
    String statusControl;//•	признак контрольности;
    String controller;//•	контролер поручения.
}
 class Incoming extends Document{
    String sender;//•	отправитель;
    String addressee;//•	адресат;
    int oNumber;//•	исходящий номер;
    Date oDate;//•	исходящая дата регистрации.
}
 class Outgoing extends Document{
    String addressee;//•	адресат;
    String delivery;//•	способ доставки.
}

interface Storable{
    void getNumber();
    void getName();
}
//class DocCreater{

//}
